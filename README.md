My online hosted resume homepage hosted with [jsonresume](https://jsonresume.org/)

# Workflow
Despite that I'm using [jsonresume](https://jsonresume.org/) to create a html page, I write the resume itself in yaml as this is more human readable. The yaml file just gets converted to json before creating an html.

# Development

```bash
# install dependencies
apt-get update
apt-get install -y jq python3 python3-pip
pip3 install yq
npm install -g --unsafe-perm resume-cli     # for global installation of resume
npm install         # install the themes which are defined in package.json
```

```bash
# starting a local server
resume serve --theme ./node_modules/jsonresume-theme-elegant
```

```bash
# converting the yaml resume to json
yq . resume.yml > resume.json
```

```bash
# keep watching the yaml resume and convert to json on any file change
inotify-hookable -t 100 -f resume.yml -c 'yq . resume.yml > resume.json'
```
