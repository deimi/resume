FROM node:lts-buster

RUN apt-get update && \
    apt-get install -y jq python3 python3-pip && \
    pip3 install yq && \
    npm install -g --unsafe-perm resume-cli
